#!/bin/bash

PROG=calc
TESTNAME=Calc


setenv()
{
	pwd | grep build
	if [ $? -eq 1 ]; then
		cd build
	fi
}

create()
{
	mkdir -p {build,lib,src,tst}
	if [ ! -d lib/googletest ]; then
		cp -r ~/proj/tests/googletest lib
	fi
	tree -L 2 .
}

build()
{
	echo prog is  ${PROG}
	# makefile generators = Unix Makefiles
	setenv
	cmake .. -DCMAKE_BUILD_TYPE=Debug -G "Unix Makefiles"
	make
	make all
}

run()
{
	setenv
	src/gtest_${PROG}_run
	# iterate 2 times
	tst/gtest_${PROG}_tst --gtest_repeat=2 
}

log()
{
	setenv
	tst/gtest_${PROG}_tst --gtest_output="xml:report.xml" gtest_filter=${TESTNAME}Test.*-${TESTNAME}Test.Zero*
	google-chrome report.xml
}

debug()
{
	setenv
	tst/gtest_${PROG}_tst --gtest_break_on_failure
}

create
build
run
debug
log
