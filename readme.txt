https://developer.ibm.com/technologies/systems/articles/au-googletestingframework/#list1

https://ommune.com/kb/pushing-a-new-project-directory-to-bitbucket-repository/

steps to push a local repo to bitbucket
----------------------------------------
git commit -am "initial commit"
1. Initialize the directory under source control.

git init
2. Add the existing files to the repository.

git add .
3. Commit the files.

git commit -m "message"
4. Log into Bitbucket.
5. Create a new repository.
6. Locate the Repository setup page.



7. Choose I have an existing project.
8. Follow the directions in the pane for your repository.

cd /path/to/my/repo
git remote add origin https://your-username@bitbucket.org/ommunedevelopers/repository-name.git
git push -u origin --all 
# pushes up the repo and its refs for the first time

git push -u origin --tags
# pushes up any tags

